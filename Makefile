.PHONY: default
default: clean build run

container_name=pg
pg_opts=-c wal_level=archive
pg_opts+=-c shared_buffers=256MB

.PHONY: build
build:
	docker build -t $(container_name) .

.PHONY: clean
clean:
	-docker stop $(container_name)
	-docker rm -v $(container_name)

.PHONY: run
run:
	docker run -d --name $(container_name) $(container_name) $(pg_opts)

.PHONY: exec
exec:
	docker exec -it $(container_name) /bin/bash

.PHONY: check_wal_level
check_wal_level:
	docker exec -it $(container_name) psql -U postgres -c 'show wal_level;'

.PHONY: check_shared_buffers
check_shared_buffers:
	docker exec -it $(container_name) psql -U postgres -c 'show shared_buffers;'
